create database sistemapedidos;
create database sistemapedidosteste;

use sistemapedidos;
create table cliente(
  id int not null primary key auto_increment,
  cpf varchar(15) not null,
  nome varchar(30) not null,
  sobreNome varchar(50) not null,
  unique(cpf)
);

create table pedido(
  id int not null primary key auto_increment,
  id_cliente int not null, 
  data date not null,
  foreign key (id_cliente) references cliente(id)
);

create table produto(
  id int not null primary key auto_increment,
  descricao varchar(45) not null
);

create table item_do_pedido(
  id_pedido int not null,
  id_produto int not null,
  quantidade int not null,
  foreign key (id_pedido) references pedido(id),
  foreign key (id_produto) references produto(id),
  primary key (id_pedido, id_produto)
);

use sistemapedidosteste;
create table cliente(
  id int not null primary key auto_increment,
  cpf varchar(15) not null,
  nome varchar(30) not null,
  sobreNome varchar(50) not null,
  unique(cpf)
);

create table pedido(
  id int not null primary key auto_increment,
  id_cliente int not null, 
  data date not null,
  foreign key (id_cliente) references cliente(id)
);

create table produto(
  id int not null primary key auto_increment,
  descricao varchar(45) not null
);

create table item_do_pedido(
  id_pedido int not null,
  id_produto int not null,
  quantidade int not null,
  foreign key (id_pedido) references pedido(id),
  foreign key (id_produto) references produto(id),
  primary key (id_pedido, id_produto)
);
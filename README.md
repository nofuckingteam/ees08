#Trabalho Disciplina#
#####Turma 2016 - EES008 - Programação em JAVA para Web#####
##Requisitos##
+ git version 2.1.4
+ mysql  Ver 14.14 Distrib 5.7.14
+ Java Jdk 1.8.0_102
+ Apache Maven 3.3.9
+ WildFly Full 10.1.0.Final
##Instalação##
###Baixar submódulos###
    $ git clone git@bitbucket.org:nofuckingteam/ees08.git
    $ cd ees08
    $ git submodule update --init --remote
###Instalar base de dados###
    $ mysql < mysql.sql
###Alterar propriedades para conexão jdbc###
    /sistemapedidos/src/main/resources/config.properties
    /sistemapedidos/src/test/resources/config.properties
###Montar projeto###
    $mvn clean install package
###Deploy###
    $ cp sistemapedidos-web/target/sistemapedidos-web-0.0.1-SNAPSHOT.war $JBOSS_HOME/standalone/deployments/sistemapedidos-web-0.0.1-SNAPSHOT.war  
##Rodar SA##
    $ $JBOSS_HOME/bin/standalone.sh

##Acessar sistema##
[sistema pedidos web](http://localhost:8080/sistemapedidos-web-0.0.1-SNAPSHOT/)